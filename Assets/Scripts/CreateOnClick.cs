using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;


public class CreateOnClick : MonoBehaviour
{
    public GameObject aCopier;
    public InputAction instantiateCube;
    public float intensity = 100.0f;
    public int offset = 3;
    // Start is called before the first frame update
    void Start()
    {
        instantiateCube.Enable();
    }

    // Update is called once per frame
    void Update()
    {
        if (instantiateCube.triggered)
        {
            GameObject newObject = Instantiate(aCopier);
            newObject.GetComponent<Renderer>().material.color = Color.yellow;
            newObject.transform.position = Camera.main.transform.position + Camera.main.transform.forward * offset;
            newObject.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * intensity, ForceMode.Impulse);

        }
    }
}
