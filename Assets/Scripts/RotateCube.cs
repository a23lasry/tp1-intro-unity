using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class RotateCube : MonoBehaviour
{
    public float speed = 3.0f;
    public InputAction activateRotation;
    private bool rotate = false;
    // Start is called before the first frame update
    void Start()
    {
        activateRotation.Enable();

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 newRotation = new Vector3(0, 3, 0);
       

        if (activateRotation.triggered)
        {
            rotate = !rotate;
            Debug.Log("Rotation SHIFTED");
        }
        if (rotate)
        {
            transform.Rotate(newRotation * Time.deltaTime * speed);
            Debug.Log("Rotation PERFORMING");

        }
    }
}
