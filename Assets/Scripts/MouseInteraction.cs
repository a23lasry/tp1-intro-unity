using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseInteraction : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public float intensity = 100.0f;
    private Rigidbody _rigidbody;
    private Renderer _renderer;
    private Color _color;
    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _renderer = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
   

    public void OnPointerClick(PointerEventData eventData)
    {
        _rigidbody.AddForce(Camera.main.transform.forward * intensity);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        _color = _renderer.material.color;
        _renderer.material.color = Color.red;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _renderer.material.color = _color;
    }
}
